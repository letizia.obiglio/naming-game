import { useState } from "react";
import "./App.css";

function Haut({ ttr }) {
  return <h1>{ttr}</h1>;
}
function Component({ thg, onClick }) {
  return (
    <div>
      <p>{thg}</p>
      <button onClick={() => onClick(thg)}>toto</button>
    </div>
  );
}

const App = () => {
  const handleMot = (chiffre) => {
    if (parseInt(chiffre) % 2 === 0) {
      return `Salad`;
    } else {
      return `BK`;
    }
  };
  const [a, setA] = useState("on mange ou");
  const is_change = () => {
    const x = Math.random() * 100;
    setA(handleMot(x));
  };

  return (
    <div className="App">
      <header className="App-header">
        <Haut ttr="Welcome" />
        <Component thg={a} onClick={is_change} />
      </header>
    </div>
  );
};

export default App;
